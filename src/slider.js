/*jshint  maxlen:false, expr:true*/
var SVG = require('svg.js');
var inherits = require('inherits');
var EventEmitter = require('eventemitter3');

var Slider = function(container) {
    var self = this;
    var width = '100%';
    var height = 22;
    var margin = 27;
    var draw = SVG(container).size(width, height + margin).style('overflow', 'hidden');

    var space = 3;
    var barWidth;
    var barHeight = 3;
    var extent, line0, line1;

    var handleWidth = 10;
    var handleHeight = 17;
    var left = draw.group().width(handleWidth);
    var leftPath = draw.path('M0,0 L' + handleWidth + ',0 10,10 0, 17')
        .y(height - handleHeight - barHeight)
        .attr({fill: 'white'});

    var right = draw.group().width(handleWidth);
    var rightPath = draw.path('M0,0 L' + handleWidth + ',0 10,17 0, 10')
        .y(height - handleHeight - barHeight)
        .attr({fill: 'white'}).translate(-handleWidth);

    left.add(leftPath);
    right.add(rightPath);

    var size;
    var value;

    var startX = 0;
    var dx0 = 0;
    var target;

    var plugins = [];
    var pluginsInited = false;

    var isAnimating;
    var animationDuration = 200;
    var animationEasing = '>';

    var isSingle = false;

    var constrain = function(x) {
        var min, max;

        if (isSingle) {
            min = barWidth/2;
            max = draw.width() - barWidth/2;
        } else {
            if (target === left) {
                min = 0;
                max = right.x() - 2 * handleWidth;
            } else {
                min = left.x() + left.width() + handleWidth;
                max = draw.width();
            }
        }

        if (x < min) { return min; }
        if (x > max) { return max; }

        return x;
    };

    var roundByBar = function (x) {
        return Math.round(x / (barWidth + space));
    };

    var floorByBar = function (x) {
        return Math.floor(x / (barWidth + space));
    };

    var userSelect = function (value) {
        document.body.style.userSelect = value;
        document.body.style.webkitUserSelect = value;
        document.body.style.MozUserSelect = value;
        document.body.style.msUserSelect = value;
    };

    var onDragStart = function(e) {
        e.stopImmediatePropagation();
        target = this;
        startX = dx0 = e.pageX;
        stopLooping();
        SVG.on(document, 'mousemove', onDragMove);
        SVG.on(document, 'mouseup', onDragEnd);
        userSelect('none');
    };

    var onDragMove = function(e) {
        e.stopImmediatePropagation();
        var dx = e.pageX - dx0;
        var x;
        if (isSingle) {
            x = Math.round(constrain(left.x() + dx));
            left.move(x, 0);
            right.move(x, 0);
            extent.move(left.x() - barWidth/2);
            extent.width(barWidth);
        } else {
            x = constrain(target.x() + dx);
            target.move(x, 0);
            extent.move(left.x());
            extent.width(right.x() - left.x());
        }

        var currentValue = isSingle ?
            floorByBar(left.x()) :
            [roundByBar(left.x()), roundByBar(right.x())];

        notify(Slider.CHANGE_EVENT, currentValue);

        dx0 = e.pageX;
    };

    var onDragEnd = function(e) {
        e.stopImmediatePropagation();
        if (isSingle) {
            single(floorByBar(left.x()), true);
        } else {
            range(Math.min(roundByBar(left.x()), size - 1),
                roundByBar(right.x()), true);
        }
        SVG.off(document, 'mousemove', onDragMove);
        SVG.off(document, 'mouseup', onDragEnd);
        userSelect('text');
    };

    var notify = function(event, value) {
        self.emit(event, value);
    };

    var stopLooping = function() {
        if (activeLoop) {
            activeLoop.pause();
        }
        activeLoop = null;
    };

    var loop = function() {
        var animation;
        var prevTime = 0;
        var duration = 300;
        var i = isSingle ? value + 1 : value[1] - value[0];
        var mod = isSingle ? size : size - value[0];

        var play = function() {
            start();
            self.emit('loop:play');
        };

        var start = function() {
            animation = requestAnimationFrame(function (time) {
                if (time - prevTime > duration) {
                    if (isSingle) {
                        single(i % mod, true);
                    } else {
                        range(value[0], value[0] + 1 + i % mod, true);
                    }
                    i += 1;
                    prevTime = time;
                }
                start();
            });
        };

        var pause = function() {
            cancelAnimationFrame(animation);
            self.emit('loop:pause');
        };

        return {
            play: play,
            pause: pause
        };
    };

    var animate = function(target) {
        isAnimating = true;
        return target.animate(animationDuration, animationEasing).after(function() {
            isAnimating = false;
        });
    };

    var change = function (x0, x1, x2, width, animated) {
        if (animated) {
            animate(left).move(x0, 0);
            animate(right).move(x1, 0);
            animate(extent)
                .move(x2, 0)
                .attr({width: width});
        } else {
            left.move(x0, 0);
            right.move(x1, 0);
            extent.move(x2, 0);
            extent.width(width);
        }

        notify(Slider.CHANGE_EVENT, value);
    };

    var range = function(min, max, animated) {
        if (isSingle) {
            isSingle = false;
            stopLooping();
        }

        if (min >= max) {
            max = min + 1;
        }

        var x0 = min * (barWidth + space);
        var x1 = max * (barWidth + space);

        value = [min, max];

        change(x0, x1 - space, x0, Math.abs(x1 - x0), animated);
    };

    var single = function(index, animated) {
        if (!isSingle) {
            isSingle = true;
            stopLooping();
        }

        var x = (barWidth + space) * index;

        value = index;

        change(
            Math.round(x + barWidth/2),
            Math.round(x + barWidth/2),
            x, barWidth, animated
        );
    };

    var createBar = function () {
        var bar = draw.group();

        bar
            .rect()
            .y(height - barHeight)
            .style({
                'shape-rendering': 'crispEdges'
            });

        bar.rect().y(height - 4 * barHeight);
        bar.click(onBarClick);

        return bar;
    };

    var onBarClick = function (e) {
        var index = this.data('index');
        if (isSingle) {
            single(index, true);
        } else {
            var offsetX = e.pageX - draw.node.getBoundingClientRect().left;
            var dx1 = Math.abs(left.x() - offsetX);
            var dx2 = Math.abs(right.x() - offsetX);
            if (dx1 < dx2) {
                value[0] = index;
            } else {
                value[1] = index + 1;
            }
            range(value[0], value[1], true);
        }
    };

    var set = function(newSize) {
        size = newSize;
        barWidth = Math.round(container.clientWidth / size) - space;

        if (!extent) {
            extent = draw.rect(0, height);

            line0 = draw.group();
            line1 = draw.group();

            for (var i = 0, l = size; i < l; ++i) {
               line0.add(createBar(i).data('index', i));
               line1.add(createBar(i).data('index', i));
            }

            line1.clipWith(extent);
        }

        line0.each(function (i) {
            this.x(i * (barWidth + space));
            this.children()[1].width(barWidth);
            this.children()[1].height(7 * barHeight);
            this.children()[1].fill('transparent');
            this.children()[0].width(barWidth);
            this.children()[0].height(barHeight);
            this.children()[0].fill('gray');
        });

        line1.each(function (i) {
            this.x(i * (barWidth + space));
            this.children()[1].width(barWidth);
            this.children()[1].height(7 * barHeight);
            this.children()[1].fill('transparent');
            this.children()[0].width(barWidth);
            this.children()[0].height(barHeight);
            this.children()[0].fill('white');
        });

        if (!pluginsInited && plugins.length) {
            plugins.forEach(function (plugin) { plugin(this); }.bind(this));
            pluginsInited = true;
        }

        left.front();
        right.front();

        draw.width(size * (barWidth + space) - space);

        notify(Slider.SET_EVENT);
    };

    var onWindowResize = function() {
        if (value === undefined) { return; }
        this.set(size);
        if (isSingle) {
            this.single(value);
        } else {
            this.range(value[0], value[1]);
        }
    };

    left.on('mousedown', onDragStart);
    right.on('mousedown', onDragStart);

    SVG.on(window, 'resize', onWindowResize.bind(this));

    this.single = single;
    this.range = range;
    this.set = set;
    this.size = function () { return size; };
    this.barWidth = function () { return barWidth; };
    this.space = function () { return space; };
    this.height = function () { return height; };
    this.draw = function () { return draw; };
    this.plug = function (plugin) {
        plugins.push(plugin);
    };

    var activeLoop;
    this.loop = function() {
        return activeLoop || (activeLoop = loop());
    };
    this.stopLooping = stopLooping;
};

Slider.CHANGE_EVENT = 'change';
Slider.SET_EVENT = 'set';

inherits(Slider, EventEmitter);

Slider.create = function(container) {
    return new Slider(container);
};

module.exports = Slider;

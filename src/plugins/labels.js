module.exports = function (s) {
    var months = ['Mar.', 'Apr.', 'May', 'June', 'July', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.', 'Jan.', 'Feb.'];
    var years = ['2014', '2015'];

    var height = s.height(), draw = s.draw();

    var updateLabels = function () {
        var children = monthLabels.children();
        var monthLabel;
        var i, l = Math.min(s.size(), children.length);
        var barWidth = s.barWidth();
        var space = s.space();

        for (i = 0; i < l; i++) {
            monthLabel = children[i];
            monthLabel.x(i * (barWidth + space) + barWidth / 2);
            monthLabel.font({size: barWidth > 20 ? 11 : barWidth > 15 ? 9 : 8});
        }
    };

    var updateYearLabels = function () {
        var barWidth = s.barWidth();
        var space = s.space();
        var size = s.size();
        var children = yearLabels.children();
        children[0].x(barWidth / 2);
        children[1].x((size - 2) * (barWidth + space) + barWidth / 2);
    };

    var monthLabels = s.draw().group();
    months.forEach(function (month) {
        monthLabels.text(month)
            .style({
                'fill': 'white',
                'text-anchor': 'middle'
            }).dy('1em').y(height);
    });

    var yearLabels = draw.group();
    years.forEach(function (year) {
        yearLabels.text(year)
            .style({
                'fill': 'white',
                'text-anchor': 'middle'
            }).font({
                size: 8
            })
            .dy('1em').y(height + 17);
    });

    s.on('set', function () {
        updateLabels();
        updateYearLabels();
    });
};

var gulp = require('gulp');
var webpack = require('gulp-webpack');
var liveServer = require('live-server');

gulp.task('watch', function() {
    gulp.watch('src/*', ['webpack', 'webpack-test']);
    gulp.watch('test/src/*', ['webpack-test']);
});

gulp.task('webpack', function () {
    return gulp.src('src/slider.js')
        .pipe(webpack({
            output: { filename: 'slider.js' }
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('webpack-test', function () {
    return gulp.src('test/src/test.js')
        .pipe(webpack({
            output: { filename: 'test.js' }
        }))
        .pipe(gulp.dest('test/dist'));
});

gulp.task('server', function () {
    liveServer.start({
        port: 8080,
        host: '0.0.0.0',
        root: 'test/dist',
        open: true
    });
});

gulp.task('build', ['webpack']);
gulp.task('default', ['webpack-test', 'watch', 'server']);


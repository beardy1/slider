var Slider = require('../../src/slider');

var slider1 = Slider.create(document.getElementById('slider-1'));
slider1.plug(require('../../src/plugins/labels.js'));
slider1.set(12);
slider1.single(2);

var slider2 = Slider.create(document.getElementById('slider-2'));
slider2.set(12);
slider2.range(2, 6);

var slider3 = Slider.create(document.getElementById('slider-3'));
slider3.set(12);
slider3.range(2, 6);
